﻿#include <Windows.h>
#include <d3d12.h>
#include <dxgi1_6.h>
#include <vector>
#include <string>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <DirectXTex.h>
#include<wrl.h>

using namespace DirectX;
using namespace Microsoft::WRL;

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

LRESULT CALLBACK WindowProc(
    HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

//# Windowsアプリでのエントリーポイント(main関数)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    HRESULT result;
    ComPtr<ID3D12Device> dev;
    ComPtr<IDXGIFactory6> dxgiFactory;
    ComPtr<IDXGISwapChain4> swapchain;
    ComPtr<ID3D12CommandAllocator> cmdAllocator;
    ComPtr<ID3D12GraphicsCommandList> cmdList;
    ComPtr<ID3D12CommandQueue> cmdQueue;
    ComPtr<ID3D12DescriptorHeap> rtvHeaps;

#ifdef _DEBUG
    //デバッグレイヤーをオンに
    ComPtr<ID3D12Debug> debugController;
    if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
    {
        debugController->EnableDebugLayer();
    }
#endif

    // コンソールへの文字出力
    OutputDebugStringA("Hello,DirectX!!\n");
    // ウィンドウサイズ
    const int window_width = 1280;  // 横幅
    const int window_height = 720;  // 縦幅

    WNDCLASSEX w{}; // ウィンドウクラスの設定
    w.cbSize = sizeof(WNDCLASSEX);
    w.lpfnWndProc = (WNDPROC)WindowProc; // ウィンドウプロシージャを設定
    w.lpszClassName = L"DirectXGame"; // ウィンドウクラス名
    w.hInstance = GetModuleHandle(nullptr); // ウィンドウハンドル
    w.hCursor = LoadCursor(NULL, IDC_ARROW); // カーソル指定

    // ウィンドウクラスをOSに登録
    RegisterClassEx(&w);
    // ウィンドウサイズ{ X座標 Y座標 横幅 縦幅 }
    RECT wrc = { 0, 0, window_width, window_height };
    AdjustWindowRect(&wrc, WS_OVERLAPPEDWINDOW, false); // 自動でサイズ補正

    // ウィンドウオブジェクトの生成
    HWND hwnd = CreateWindow(w.lpszClassName, // クラス名
        L"DirectXGame",         // タイトルバーの文字
        WS_OVERLAPPEDWINDOW,        // 標準的なウィンドウスタイル
        CW_USEDEFAULT,              // 表示X座標（OSに任せる）
        CW_USEDEFAULT,              // 表示Y座標（OSに任せる）
        wrc.right - wrc.left,       // ウィンドウ横幅
        wrc.bottom - wrc.top,   // ウィンドウ縦幅
        nullptr,                // 親ウィンドウハンドル
        nullptr,                // メニューハンドル

        w.hInstance,            // 呼び出しアプリケーションハンドル
        nullptr);               // オプション

    // ウィンドウ表示
    ShowWindow(hwnd, SW_SHOW);

    MSG msg{};  // メッセージ

// DirectX初期化処理　ここから
     // ウィンドウクラスを登録解除
    UnregisterClass(w.lpszClassName, w.hInstance);

    // DXGIファクトリーの生成
    result = CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory));
    // アダプターの列挙用
    std::vector<ComPtr<IDXGIAdapter1>> adapters;
    // ここに特定の名前を持つアダプターオブジェクトが入る
    ComPtr<IDXGIAdapter1> tmpAdapter;
    for (int i = 0;
        dxgiFactory->EnumAdapters1(i, &tmpAdapter) != DXGI_ERROR_NOT_FOUND;
        i++)
    {
        adapters.push_back(tmpAdapter); // 動的配列に追加する
    }

    for (int i = 0; i < adapters.size(); i++)
    {
        DXGI_ADAPTER_DESC1 adesc;
        adapters[i]->GetDesc1(&adesc);  // アダプターの情報を取得

        // ソフトウェアデバイスを回避
        if (adesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
            continue;
        }

        std::wstring strDesc = adesc.Description;   // アダプター名
        // Intel UHD Graphics（オンボードグラフィック）を回避
        if (strDesc.find(L"Intel") == std::wstring::npos)
        {
            tmpAdapter = adapters[i];   // 採用
            break;
        }
    }

    // 対応レベルの配列
    D3D_FEATURE_LEVEL levels[] =
    {
        D3D_FEATURE_LEVEL_12_1,
        D3D_FEATURE_LEVEL_12_0,
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };

    D3D_FEATURE_LEVEL featureLevel;

    for (int i = 0; i < _countof(levels); i++)
    {
        // 採用したアダプターでデバイスを生成
        result = D3D12CreateDevice(tmpAdapter.Get(), levels[i], IID_PPV_ARGS(&dev));
        if (result == S_OK)
        {
            // デバイスを生成できた時点でループを抜ける
            featureLevel = levels[i];
            break;
        }
    }

    // コマンドアロケータを生成
    result = dev->CreateCommandAllocator(
        D3D12_COMMAND_LIST_TYPE_DIRECT,
        IID_PPV_ARGS(&cmdAllocator));

    // コマンドリストを生成
    result = dev->CreateCommandList(
        0,
        D3D12_COMMAND_LIST_TYPE_DIRECT,
        cmdAllocator.Get(), nullptr,
        IID_PPV_ARGS(&cmdList));

    // 標準設定でコマンドキューを生成
    D3D12_COMMAND_QUEUE_DESC cmdQueueDesc{};

    dev->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&cmdQueue));

    // 各種設定をしてスワップチェーンを生成
    DXGI_SWAP_CHAIN_DESC1 swapchainDesc{};
    swapchainDesc.Width = 1280;
    swapchainDesc.Height = 720;
    swapchainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // 色情報の書式
    swapchainDesc.SampleDesc.Count = 1; // マルチサンプルしない
    swapchainDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER; // バックバッファ用
    swapchainDesc.BufferCount = 2;  // バッファ数を２つに設定
    swapchainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD; // フリップ後は破棄
    swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    //IDXGISwapChain1のComPtrを用意
    ComPtr<IDXGISwapChain1> swapchain1;

    result = dxgiFactory->CreateSwapChainForHwnd(
        cmdQueue.Get(),
        hwnd,
        &swapchainDesc,
        nullptr,
        nullptr,
        &swapchain1);

    //生成したIDXGISwapChainのオブジェクトをIDXGISwapChain4に変換する
    swapchain1.As(&swapchain);

    // 各種設定をしてデスクリプタヒープを生成
    D3D12_DESCRIPTOR_HEAP_DESC heapDesc{};
    heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV; // レンダーターゲットビュー
    heapDesc.NumDescriptors = 2;    // 裏表の２つ
    dev->CreateDescriptorHeap(&heapDesc,
        IID_PPV_ARGS(&rtvHeaps));

    // 裏表の２つ分について
    std::vector<ID3D12Resource*> backBuffers(2);
    for (int i = 0; i < 2; i++)
    {
        // スワップチェーンからバッファを取得
        result = swapchain->GetBuffer(i, IID_PPV_ARGS(&backBuffers[i]));
        // デスクリプタヒープのハンドルを取得
        D3D12_CPU_DESCRIPTOR_HANDLE handle = rtvHeaps->GetCPUDescriptorHandleForHeapStart();
        // 裏か表かでアドレスがずれる
        handle.ptr += i * dev->GetDescriptorHandleIncrementSize(heapDesc.Type);
        // レンダーターゲットビューの生成
        dev->CreateRenderTargetView(
            backBuffers[i],
            nullptr,
            handle);
    }

    //深度バッファリソース設定
    D3D12_RESOURCE_DESC depthResDesc{};
    depthResDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    depthResDesc.Width = window_width;
    depthResDesc.Height = window_height;
    depthResDesc.DepthOrArraySize = 1;
    depthResDesc.Format = DXGI_FORMAT_D32_FLOAT;
    depthResDesc.SampleDesc.Count = 1;
    depthResDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

    //深度値用ヒーププロパティ
    D3D12_HEAP_PROPERTIES depthHeapProp{};
    depthHeapProp.Type = D3D12_HEAP_TYPE_DEFAULT;
    //深度値のクリア設定
    D3D12_CLEAR_VALUE depthClearValue{};
    depthClearValue.DepthStencil.Depth = 1.0f;
    depthClearValue.Format = DXGI_FORMAT_D32_FLOAT;

    //深度バッファリソース生成
    ComPtr<ID3D12Resource> depthBuffer;
    result = dev->CreateCommittedResource(
        &depthHeapProp,
        D3D12_HEAP_FLAG_NONE,
        &depthResDesc,
        D3D12_RESOURCE_STATE_DEPTH_WRITE,
        &depthClearValue,
        IID_PPV_ARGS(&depthBuffer)
    );

    //深度ビュー用デスクリプタヒープ作成
    D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc{};
    dsvHeapDesc.NumDescriptors = 1;
    dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
    ComPtr<ID3D12DescriptorHeap> dsvHeap;
    result = dev->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&dsvHeap));

    //深度ビュー生成
    D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
    dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
    dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
    dev->CreateDepthStencilView(
        depthBuffer.Get(),
        &dsvDesc,
        dsvHeap->GetCPUDescriptorHandleForHeapStart()
    );

    // フェンスの生成
    ComPtr<ID3D12Fence> fence;
    UINT64 fenceVal = 0;

    result = dev->CreateFence(fenceVal, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence));

    IDirectInput8* dinput = nullptr;
    result = DirectInput8Create(w.hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8,
        (void**)&dinput, nullptr);

    IDirectInputDevice8* devkeyboard = nullptr;
    result = dinput->CreateDevice(GUID_SysKeyboard, &devkeyboard, NULL);

    result = devkeyboard->SetDataFormat(&c_dfDIKeyboard);

    result = devkeyboard->SetCooperativeLevel(
        hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE | DISCL_NOWINKEY
    );

    // DirectX初期化処理　ここまで
    // 
    //描画初期化処理　ここから
 
    //分割数()

    struct Vertex {
        XMFLOAT3 pos;
        XMFLOAT3 normal;
        XMFLOAT2 uv;
    };

    Vertex vertices[] = {
        //前
        {{-5.0f, -5.0f, -5.0f}, {}, {0.0f, 1.0f}},//0
        {{-5.0f, +5.0f, -5.0f}, {}, {0.0f, 0.0f}},//1
        {{+5.0f, -5.0f, -5.0f}, {}, {1.0f, 1.0f}},//2
        {{+5.0f, +5.0f, -5.0f}, {}, {1.0f, 0.0f}},//3
        //後
        {{-5.0f, -5.0f, +5.0f}, {}, {0.0f, 1.0f}},//4
        {{-5.0f, +5.0f, +5.0f}, {}, {0.0f, 0.0f}},//5
        {{+5.0f, -5.0f, +5.0f}, {}, {1.0f, 1.0f}},//6
        {{+5.0f, +5.0f, +5.0f}, {}, {1.0f, 0.0f}},//7
        //左
        {{-5.0f, -5.0f, -5.0f}, {}, {0.0f, 1.0f}},//0
        {{-5.0f, -5.0f, +5.0f}, {}, {0.0f, 0.0f}},//4
        {{-5.0f, +5.0f, -5.0f}, {}, {1.0f, 1.0f}},//1
        {{-5.0f, +5.0f, +5.0f}, {}, {1.0f, 0.0f}},//5
        //右
        {{+5.0f, -5.0f, -5.0f}, {}, {0.0f, 1.0f}},//2
        {{+5.0f, -5.0f, +5.0f}, {},{0.0f, 0.0f}},//6
        {{+5.0f, +5.0f, -5.0f}, {}, {1.0f, 1.0f}},//3
        {{+5.0f, +5.0f, +5.0f}, {}, {1.0f, 0.0f}},//7
        //下
        {{-5.0f, -5.0f, -5.0f}, {}, {0.0f, 1.0f}},//0
        {{+5.0f, -5.0f, -5.0f}, {}, {0.0f, 0.0f}},//2
        {{-5.0f, -5.0f, +5.0f}, {}, {1.0f, 1.0f}},//4
        {{+5.0f, -5.0f, +5.0f}, {}, {1.0f, 0.0f}},//6
        //上
        {{-5.0f, +5.0f, -5.0f}, {}, {0.0f, 1.0f}},//1
        {{+5.0f, +5.0f, -5.0f}, {}, {0.0f, 0.0f}},//3
        {{-5.0f, +5.0f, +5.0f}, {}, {1.0f, 1.0f}},//5
        {{+5.0f, +5.0f, +5.0f}, {}, {1.0f, 0.0f}},//7
    };

    unsigned short indices[] = {
        0, 1, 2,
        2, 1, 3,

        6, 7, 4,
        4, 7, 5,

        8, 9, 10,
        10, 9, 11,

        12, 14, 13,
        13, 14, 15,

        16, 17, 18,
        18, 17, 19,

        20, 22, 21,
        21, 22, 23,
    };

    for (int i = 0; i < _countof(indices) / 3; i++) {
        //三角形ひとつごとに計算していく

        //三角形のインデックスを取り出して、一時的な変数に入れる
        unsigned short Tempo1 = indices[i * 3 + 0];
        unsigned short Tempo2 = indices[i * 3 + 1];
        unsigned short Tempo3 = indices[i * 3 + 2];

        XMVECTOR p0 = XMLoadFloat3(&vertices[Tempo1].pos);
        XMVECTOR p1 = XMLoadFloat3(&vertices[Tempo2].pos);
        XMVECTOR p2 = XMLoadFloat3(&vertices[Tempo3].pos);

        XMVECTOR v1 = XMVectorSubtract(p1, p0);
        XMVECTOR v2 = XMVectorSubtract(p2, p0);

        XMVECTOR normal = XMVector3Cross(v1, v2);

        normal = XMVector3Normalize(normal);

        XMStoreFloat3(&vertices[Tempo1].normal, normal);
        XMStoreFloat3(&vertices[Tempo2].normal, normal);
        XMStoreFloat3(&vertices[Tempo3].normal, normal);
    }

    // 頂点データ全体のサイズ = 頂点データ一つ分のサイズ * 頂点データの要素数
    UINT sizeVB = static_cast<UINT>(sizeof(Vertex) * _countof(vertices));

    // 頂点バッファの設定
    D3D12_HEAP_PROPERTIES heapprop{};   // ヒープ設定
    heapprop.Type = D3D12_HEAP_TYPE_UPLOAD; // GPUへの転送用

    D3D12_RESOURCE_DESC resdesc{};  // リソース設定
    resdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    resdesc.Width = sizeVB; // 頂点データ全体のサイズ
    resdesc.Height = 1;
    resdesc.DepthOrArraySize = 1;
    resdesc.MipLevels = 1;
    resdesc.SampleDesc.Count = 1;
    resdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

    // 頂点バッファの生成
    ComPtr<ID3D12Resource> vertBuff;
    result = dev->CreateCommittedResource(
        &heapprop, // ヒープ設定
        D3D12_HEAP_FLAG_NONE,
        &resdesc, // リソース設定
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&vertBuff));

    // 頂点バッファビューの作成
    D3D12_VERTEX_BUFFER_VIEW vbView{};
    vbView.BufferLocation = vertBuff->GetGPUVirtualAddress();
    vbView.SizeInBytes = sizeVB;
    vbView.StrideInBytes = sizeof(Vertex);

    ID3DBlob* vsBlob = nullptr; // 頂点シェーダオブジェクト
    ID3DBlob* psBlob = nullptr; // ピクセルシェーダオブジェクト
    ID3DBlob* errorBlob = nullptr; // エラーオブジェクト

    // 頂点シェーダの読み込みとコンパイル
    result = D3DCompileFromFile(
        L"BasicVS.hlsl",  // シェーダファイル名
        nullptr,
        D3D_COMPILE_STANDARD_FILE_INCLUDE, // インクルード可能にする
        "main", "vs_5_0", // エントリーポイント名、シェーダーモデル指定
        D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, // デバッグ用設定
        0,
        &vsBlob, &errorBlob);

    // ピクセルシェーダの読み込みとコンパイル
    result = D3DCompileFromFile(
        L"BasicPS.hlsl",   // シェーダファイル名
        nullptr,
        D3D_COMPILE_STANDARD_FILE_INCLUDE, // インクルード可能にする
        "main", "ps_5_0", // エントリーポイント名、シェーダーモデル指定
        D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, // デバッグ用設定
        0,
        &psBlob, &errorBlob);


    // 頂点レイアウト
    D3D12_INPUT_ELEMENT_DESC inputLayout[] = {
        {
            "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
            D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        }, // (1行で書いたほうが見やすい)

        {
            "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
            D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },

        {
            "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
            D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        }
    };

    // グラフィックスパイプライン設定
    D3D12_GRAPHICS_PIPELINE_STATE_DESC gpipeline{};

    gpipeline.VS.pShaderBytecode = vsBlob->GetBufferPointer();
    gpipeline.VS.BytecodeLength = vsBlob->GetBufferSize();
    gpipeline.PS.pShaderBytecode = psBlob->GetBufferPointer();
    gpipeline.PS.BytecodeLength = psBlob->GetBufferSize();

    gpipeline.SampleMask = D3D12_DEFAULT_SAMPLE_MASK; // 標準設定
    gpipeline.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;  // 背面をカリング
    gpipeline.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID; // ポリゴン内塗りつぶし
    gpipeline.RasterizerState.DepthClipEnable = true; // 深度クリッピングを有効に

    //デプスステンシルステートの設定
    gpipeline.DepthStencilState.DepthEnable = true;
    gpipeline.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
    gpipeline.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
    gpipeline.DSVFormat = DXGI_FORMAT_D32_FLOAT;

    //gpipeline.BlendState.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;  // RBGA全てのチャンネルを描画
    //レンダーターゲットのブレンド設定
    D3D12_RENDER_TARGET_BLEND_DESC& blenddesc = gpipeline.BlendState.RenderTarget[0];
    blenddesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

    //共通設定
    //blenddesc.BlendEnable = true;
    //blenddesc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
    //blenddesc.SrcBlendAlpha = D3D12_BLEND_ONE;
    //blenddesc.DestBlendAlpha = D3D12_BLEND_ZERO;

    //加算
    //blenddesc.BlendOp = D3D12_BLEND_OP_ADD;
    //blenddesc.SrcBlend = D3D12_BLEND_ONE;
    //blenddesc.DestBlend = D3D12_BLEND_ONE;

    //減算
    //blenddesc.BlendOp = D3D12_BLEND_OP_REV_SUBTRACT;
    //blenddesc.SrcBlend = D3D12_BLEND_ONE;
    //blenddesc.DestBlend = D3D12_BLEND_ONE;

    //反転
    //blenddesc.BlendOp = D3D12_BLEND_OP_ADD;
    //blenddesc.SrcBlend = D3D12_BLEND_INV_DEST_COLOR;
    //blenddesc.DestBlend = D3D12_BLEND_ZERO;

    //半透明
    //blenddesc.BlendOp = D3D12_BLEND_OP_ADD;
    //blenddesc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
    //blenddesc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;


    gpipeline.InputLayout.pInputElementDescs = inputLayout;
    gpipeline.InputLayout.NumElements = _countof(inputLayout);

    gpipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    gpipeline.NumRenderTargets = 1; // 描画対象は1つ
    gpipeline.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM; // 0～255指定のRGBA
    gpipeline.SampleDesc.Count = 1; // 1ピクセルにつき1回サンプリング


    //定数バッファ用データ構造体
    struct ConstBufferData {
        XMFLOAT4 color; //色(RGBA)
        XMMATRIX mat;
    };

    //WICテクスチャのロード
    TexMetadata metadata{};
    ScratchImage scratchImg{};

    result = LoadFromWICFile(
        L"Resources/player.png",
        WIC_FLAGS_NONE,
        &metadata, scratchImg);

    const Image* img = scratchImg.GetImage(0, 0, 0);

    //ヒープ設定
    D3D12_HEAP_PROPERTIES cbheapprop{};
    cbheapprop.Type = D3D12_HEAP_TYPE_UPLOAD;

    //リソース設定
    D3D12_RESOURCE_DESC cbresdesc{};
    cbresdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    cbresdesc.Width = (sizeof(ConstBufferData) + 0xff) & ~0xff;
    cbresdesc.Height = 1;
    cbresdesc.DepthOrArraySize = 1;
    cbresdesc.MipLevels = 1;
    cbresdesc.SampleDesc.Count = 1;
    cbresdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

    //テクスチャヒープ設定
    D3D12_HEAP_PROPERTIES texHeapProp{};
    texHeapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
    texHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
    texHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;

    //リソース設定
    D3D12_RESOURCE_DESC texresDesc{};
    texresDesc.Dimension = static_cast <D3D12_RESOURCE_DIMENSION>(metadata.dimension);
    texresDesc.Format = metadata.format;
    texresDesc.Width = metadata.width;
    texresDesc.Height = (UINT)metadata.height;
    texresDesc.DepthOrArraySize = (UINT16)metadata.arraySize;
    texresDesc.MipLevels = (UINT16)metadata.mipLevels;
    texresDesc.SampleDesc.Count = 1;

    //GPUリソースの生成
    ComPtr<ID3D12Resource> constBuff;
    result = dev->CreateCommittedResource(
        &cbheapprop,
        D3D12_HEAP_FLAG_NONE,
        &cbresdesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&constBuff));

    //テクスチャバッファの生成
    ComPtr<ID3D12Resource> texBuff;
    result = dev->CreateCommittedResource(
        &texHeapProp,
        D3D12_HEAP_FLAG_NONE,
        &texresDesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&texBuff));

    //定数バッファ
    ConstBufferData* constMap = nullptr;
    result = constBuff->Map(0, nullptr, (void**)&constMap);
    constMap->color = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);

    //射影変換行列(透視投影)
    XMMATRIX matProjection = XMMatrixPerspectiveFovLH(
        XMConvertToRadians(60.0f),
        (float)window_width / window_height,
        0.1f, 1000.0f
    );

    //ビュー行列の作成
    //ビュー行列変換
    XMMATRIX matView;
    XMFLOAT3 eye(0, 10, -50); //視点座標
    XMFLOAT3 target(0, 0, 0); //注視点座標
    XMFLOAT3 up(0, 1, 0); //上方向ベクトル
    matView = XMMatrixLookAtLH(XMLoadFloat3(&eye), XMLoadFloat3(&target), XMLoadFloat3(&up));

    //ワールド変換行列
    XMMATRIX matWorld;
    matWorld = XMMatrixIdentity();
    XMFLOAT3 scale;
    XMFLOAT3 rotation;
    XMFLOAT3 position;
    XMMATRIX matScale;
    XMMATRIX matRot;
    XMMATRIX matTrans;

    scale = { 1.0f, 1.0f, 1.0f };
    rotation = { 0.0f, 0.0f, 0.0f };
    position = { -70.0f, 0.0f, 50.0f };

    constMap->mat = matWorld * matView * matProjection;
    constBuff->Unmap(0, nullptr);

    //テクスチャバッファへのデータ転送
    result = texBuff->WriteToSubresource(
        0,
        nullptr,
        img->pixels,
        (UINT)img->rowPitch,
        (UINT)img->slicePitch
    );

    //デスクリプターヒープの生成
    ComPtr<ID3D12DescriptorHeap> basicDescHeap;

    D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc{};
    descHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    descHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    descHeapDesc.NumDescriptors = 2;

    result = dev->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(&basicDescHeap));

    D3D12_CPU_DESCRIPTOR_HANDLE basicHeapHandle = basicDescHeap->GetCPUDescriptorHandleForHeapStart();

    //定数バッファビューの作成
    D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc{};
    cbvDesc.BufferLocation = constBuff->GetGPUVirtualAddress();
    cbvDesc.SizeInBytes = (UINT)constBuff->GetDesc().Width;
    dev->CreateConstantBufferView(&cbvDesc, basicHeapHandle);

    //デスクリプタ１個分アドレスを進める
    basicHeapHandle.ptr +=
        dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

    //シェーダーリソースビュー設定
    D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc{};
    srvDesc.Format = metadata.format;
    srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = 1;

    //ヒープの二番目にシェーダーリソースビュー作成
    dev->CreateShaderResourceView(texBuff.Get(),
        &srvDesc,
        basicHeapHandle
    );

    //デスクリプタテーブルの設定
    D3D12_DESCRIPTOR_RANGE descRangeCBV{};
    descRangeCBV.NumDescriptors = 1;
    descRangeCBV.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
    descRangeCBV.BaseShaderRegister = 0;
    descRangeCBV.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    D3D12_DESCRIPTOR_RANGE descRangeSRV{};
    descRangeSRV.NumDescriptors = 1;
    descRangeSRV.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
    descRangeSRV.BaseShaderRegister = 0;
    descRangeSRV.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    //ルートパラメータの設定
    D3D12_ROOT_PARAMETER rootparams[2] = {};
    //定数用
    rootparams[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    rootparams[0].DescriptorTable.pDescriptorRanges = &descRangeCBV;
    rootparams[0].DescriptorTable.NumDescriptorRanges = 1;
    rootparams[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
    //テクスチャ用
    rootparams[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    rootparams[1].DescriptorTable.pDescriptorRanges = &descRangeSRV;
    rootparams[1].DescriptorTable.NumDescriptorRanges = 1;
    rootparams[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //テクスチャサンプラーの設定
    D3D12_STATIC_SAMPLER_DESC samplerDesc{};

    samplerDesc.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerDesc.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
    samplerDesc.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
    samplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
    samplerDesc.MinLOD = 0.0f;
    samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
    samplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

    ComPtr<ID3D12RootSignature> rootsignature;

    D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc{};
    rootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
    rootSignatureDesc.pParameters = rootparams;
    rootSignatureDesc.NumParameters = _countof(rootparams);
    rootSignatureDesc.pStaticSamplers = &samplerDesc;
    rootSignatureDesc.NumStaticSamplers = 1;

    ComPtr<ID3DBlob> rootSigBlob;
    result = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1_0, &rootSigBlob, &errorBlob);
    result = dev->CreateRootSignature(0, rootSigBlob->GetBufferPointer(), rootSigBlob->GetBufferSize(), 
    IID_PPV_ARGS(&rootsignature));

    // パイプラインにルートシグネチャをセット
    gpipeline.pRootSignature = rootsignature.Get();
    ComPtr<ID3D12PipelineState> pipelinestate;
    result = dev->CreateGraphicsPipelineState(&gpipeline, IID_PPV_ARGS(&pipelinestate));

    //インデックスデータ全体のサイズ
    UINT sizeIB = static_cast<UINT>(sizeof(unsigned short) * _countof(indices));

    //インデックスバッファの設定
    ComPtr<ID3D12Resource> indexBuff;
    resdesc.Width = sizeIB;

    //インデックスバッファの生成
    result = dev->CreateCommittedResource(
        &heapprop,
        D3D12_HEAP_FLAG_NONE,
        &resdesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&indexBuff)
    );

    //インデックスバッファビューの作成
    D3D12_INDEX_BUFFER_VIEW ibView{};
    ibView.BufferLocation = indexBuff->GetGPUVirtualAddress();
    ibView.Format = DXGI_FORMAT_R16_UINT;
    ibView.SizeInBytes = sizeIB;

    //GPU上おバッファに対応した仮想メモリを取得
    unsigned short* indexMap = nullptr;
    result = indexBuff->Map(0, nullptr, (void**)&indexMap);
    //全インデックスに対して
    for (int i = 0; i < _countof(indices); i++) {
        indexMap[i] = indices[i];//インデックスをコピー
    }
    //繋がりを解除
    indexBuff->Unmap(0, nullptr);

    // GPU上のバッファに対応した仮想メモリを取得
    Vertex* vertMap = nullptr;
    result = vertBuff->Map(0, nullptr, (void**)&vertMap);

    // 全頂点に対して
    for (int i = 0; i < _countof(vertices); i++)
    {
        vertMap[i] = vertices[i];   // 座標をコピー
    }

    // マップを解除
    vertBuff->Unmap(0, nullptr);

    
    //resultのエラーが出た場合表示する
    if (FAILED(result)) {
        // errorBlobからエラー内容をstring型にコピー
        std::string errstr;
        errstr.resize(errorBlob->GetBufferSize());

        std::copy_n((char*)errorBlob->GetBufferPointer(),
            errorBlob->GetBufferSize(),
            errstr.begin());
        errstr += "\n";
        // エラー内容を出力ウィンドウに表示
        OutputDebugStringA(errstr.c_str());
        exit(1);
    }

    //描画初期化処理　ここまで

    float posX = 0.0f;
    float R = 1.0f;
    float G = 1.0f;
    float B = 1.0f;

    int BulletFlag = 0;

    while (true)  // ゲームループ
    {
        // メッセージがある？
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg); // キー入力メッセージの処理
            DispatchMessage(&msg); // プロシージャにメッセージを送る
        }

        // ✖ボタンで終了メッセージが来たらゲームループを抜ける
        if (msg.message == WM_QUIT) {
            break;
        }
        // DirectX毎フレーム処理　ここから
        //キーボード情報の取得開始
        result = devkeyboard->Acquire();

        //全キーの入力状態を取得する
        BYTE key[256] = {};
        result = devkeyboard->GetDeviceState(sizeof(key), key);

        if (key[DIK_0]) {
            OutputDebugStringA("Hit 0\n");
        }

        if (key[DIK_UP] || key[DIK_DOWN] || key[DIK_RIGHT] || key[DIK_LEFT]) {

            if (key[DIK_RIGHT]) {
                position.x += 1.0f;
            } else if (key[DIK_LEFT]) {
                position.x -= 1.0f;
            }

            if (key[DIK_UP]) {
                position.y += 1.0f;
            } else if (key[DIK_DOWN]) {
                position.y -= 1.0f;
            }

            // GPU上のバッファに対応した仮想メモリを取得
            Vertex* vertMap = nullptr;
            result = vertBuff->Map(0, nullptr, (void**)&vertMap);

            // 全頂点に対して
            for (int i = 0; i < _countof(vertices); i++)
            {
                vertMap[i] = vertices[i];   // 座標をコピー
            }

            constMap->mat = matWorld * matView * matProjection;

            // マップを解除
            vertBuff->Unmap(0, nullptr);
        }
        //1個目(自機)
        matWorld = XMMatrixIdentity();

        matScale = XMMatrixScaling(scale.x, scale.y, scale.z);
        matWorld *= matScale;
        matRot = XMMatrixIdentity();
        matRot *= XMMatrixRotationZ(XMConvertToRadians(rotation.z));
        matRot *= XMMatrixRotationX(XMConvertToRadians(rotation.x));
        matRot *= XMMatrixRotationY(XMConvertToRadians(rotation.y));
        matWorld *= matRot;

        matTrans = XMMatrixTranslation(position.x, position.y, position.z);
        matWorld *= matTrans;

        // GPU上のバッファに対応した仮想メモリを取得
        Vertex* vertMap = nullptr;
        result = vertBuff->Map(0, nullptr, (void**)&vertMap);

        // 全頂点に対して
        for (int i = 0; i < _countof(vertices); i++)
        {
            vertMap[i] = vertices[i];   // 座標をコピー
        }

        constMap->mat = matWorld * matView * matProjection;

        // マップを解除
        vertBuff->Unmap(0, nullptr);


        // バックバッファの番号を取得（2つなので0番か1番）
        UINT bbIndex = swapchain->GetCurrentBackBufferIndex();

        // １．リソースバリアで書き込み可能に変更
        D3D12_RESOURCE_BARRIER barrierDesc{};
        barrierDesc.Transition.pResource = backBuffers[bbIndex]; // バックバッファを指定
        barrierDesc.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT; // 表示から
        barrierDesc.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET; // 描画
        cmdList->ResourceBarrier(1, &barrierDesc);

        // ２．描画先指定
        // レンダーターゲットビュー用ディスクリプタヒープのハンドルを取得
        D3D12_CPU_DESCRIPTOR_HANDLE rtvH = rtvHeaps->GetCPUDescriptorHandleForHeapStart();
        rtvH.ptr += bbIndex * dev->GetDescriptorHandleIncrementSize(heapDesc.Type);
        D3D12_CPU_DESCRIPTOR_HANDLE dsvH = dsvHeap->GetCPUDescriptorHandleForHeapStart();
        cmdList->OMSetRenderTargets(1, &rtvH, false, &dsvH);

        // ３．画面クリア           R     G     B    A
        float clearColor[] = { 0.25f, 0.5f, 1.0f }; // 青っぽい色
        //float clearColor[] = { 0.0f,0.0f, 1.0f,0.0f }; //加算
        //float clearColor[] = { 1.0f,1.0f, 1.0f,0.0f }; //減算
        //float clearColor[] = { 1.0f,0.0f, 0.0f,0.0f }; //反転
        //float clearColor[] = { 0.0f,0.0f, 1.0f,0.0f }; //半透明
        cmdList->ClearRenderTargetView(rtvH, clearColor, 0, nullptr);
        cmdList->ClearDepthStencilView(dsvH, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

        // ４．描画コマンドここから
        
            cmdList->IASetIndexBuffer(&ibView);
            cmdList->SetPipelineState(pipelinestate.Get());
            cmdList->SetGraphicsRootSignature(rootsignature.Get());

            //デスクリプタヒープをセット
            ID3D12DescriptorHeap* ppHeaps[] = { basicDescHeap.Get() };
            cmdList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

            //デスクリプタヒープの先頭ハンドルを取得
            D3D12_GPU_DESCRIPTOR_HANDLE gpuDescHandle = basicDescHeap->GetGPUDescriptorHandleForHeapStart();
            //ヒープの先頭にあるCBVをルートパラメータ0番に設定
            cmdList->SetGraphicsRootDescriptorTable(0, gpuDescHandle);
            //ハンドルをひとつ進める
            gpuDescHandle.ptr += dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
            //ヒープの先頭にあるSRVをルートパラメータ1番に設定
            cmdList->SetGraphicsRootDescriptorTable(1, gpuDescHandle);

            D3D12_VIEWPORT viewport{};

            viewport.Width = window_width;
            viewport.Height = window_height;
            viewport.TopLeftX = 0;
            viewport.TopLeftY = 0;
            viewport.MinDepth = 0.0f;
            viewport.MaxDepth = 1.0f;

            cmdList->RSSetViewports(1, &viewport);

            D3D12_RECT scissorrect{};

            scissorrect.left = 0;                                       // 切り抜き座標左
            scissorrect.right = scissorrect.left + window_width;        // 切り抜き座標右
            scissorrect.top = 0;                                        // 切り抜き座標上
            scissorrect.bottom = scissorrect.top + window_height;       // 切り抜き座標下

            cmdList->RSSetScissorRects(1, &scissorrect);
            cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            cmdList->IASetVertexBuffers(0, 1, &vbView);
            cmdList->DrawIndexedInstanced(_countof(indices), 1, 0, 0, 0);

        // ４．描画コマンドここまで

        // ５．リソースバリアを戻す
        barrierDesc.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET; // 描画
        barrierDesc.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;   // 表示に
        cmdList->ResourceBarrier(1, &barrierDesc);

        // 命令のクローズ
        cmdList->Close();
        // コマンドリストの実行
        ID3D12CommandList* cmdLists[] = { cmdList.Get() }; // コマンドリストの配列
        cmdQueue->ExecuteCommandLists(1, cmdLists);
        // コマンドリストの実行完了を待つ
        cmdQueue->Signal(fence.Get(), ++fenceVal);
        if (fence->GetCompletedValue() != fenceVal) {
            HANDLE event = CreateEvent(nullptr, false, false, nullptr);
            fence->SetEventOnCompletion(fenceVal, event);
            WaitForSingleObject(event, INFINITE);
            CloseHandle(event);
        }

        cmdAllocator->Reset(); // キューをクリア
        cmdList->Reset(cmdAllocator.Get(), nullptr);  // 再びコマンドリストを貯める準備

        // バッファをフリップ（裏表の入替え）
        swapchain->Present(1, 0);

        // DirectX毎フレーム処理　ここまで
    }
    return 0;
}